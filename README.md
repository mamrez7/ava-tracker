# AVA Tracker

This paper is published in **Neural Network Journal**. If you find the code useful, please cite as follow:

@article{javanmardi2020appearance,
  title={Appearance variation adaptation tracker using adversarial network},
  author={Javanmardi, Mohammadreza and Qi, Xiaojun},
  journal={Neural Networks},
  volume={129},
  pages={334--343},
  year={2020},
  publisher={Elsevier}
}

This tracker is written in Python with Pytorch library. 

In order to reproduce the results. Please install the requirements.txt as follow:

**python -m pip install -r requirements.txt**

Download OTB100 and VOT2016 datasets. 

Run _demo_otb.py_ for the evaluation of the method on the OTB datasets.

Run _demo_vot.py_ for the evaluation of the method on the VOT2016 dataset. 

The code will be uploaded by Dec 20 2020. 
